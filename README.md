# sveltekit-global-styles-bug

This repository contains code to reproduce a bug causing global styles to dissapear on a routed page after refresh.

## How to Reproduce

1. Run the server using `pnpm run dev`

2. Check DOM for existing styles of `body` element

3. Navigate to `/contact`

4. Check DOM for existing styles of `body` element which should be the same as in second point

5. Refresh the page

## Actual Outcome

The styles that are defined as `:global()` in `src/+page.svelte` dissapeared.

## Expected Outcome

The styles persist even after the refresh.